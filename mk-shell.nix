topLevelModule:

let
  sources = import ./nix/sources.nix;

  # only needed for lib
  pkgs = import sources.nixpkgs { };

  inherit (pkgs) lib;
  inherit (lib) evalModules filter concatStringsSep showWarnings;

  evaledModules = evalModules {
    modules = [
      ./modules
      {
        _file = "<top-level>";
        imports = [ topLevelModule ];
      }
    ];
  };

  # taken from NixOS top-level.nix

  failedAssertions = map (x: x.message)
    (filter (x: !x.assertion) evaledModules.config.assertions);

  evaledModules' = if failedAssertions != [ ] then
    throw ''

      Failed assertions:
      ${concatStringsSep "\n" (map (x: "- ${x}") failedAssertions)}''
  else
    showWarnings evaledModules.config.warnings evaledModules;

in evaledModules'.config.outputs.shell
