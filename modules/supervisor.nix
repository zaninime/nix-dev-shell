{ config, lib, lib', pkgs, ... }:

with lib;

let
  cfg = config.supervisor;
  serviceNames = attrNames cfg.services;

  servicesWithCleanups = pipe cfg.services [
    (filterAttrs (_: v: v.hasCleanupScript))
    (mapAttrsToList (n: v: n))
  ];

  mkInitScript = { lib, rsync, s6, writers, stateDir, services }:
    let
      setupSvc = name: pkg: ''
        echo "Setting up ${name}"
        ${rsync}/bin/rsync -rpl --delete --chmod=u+w,g+w,o= ${pkg}/ "$SVC_DIR/${name}/"
      '';

    in writers.writeBashBin "init" ''
      set -eu

      SVC_DIR=${escapeShellArg stateDir}

      echo "Setting up base service structure"
      rm -rf "$SVC_DIR"
      mkdir -p "$SVC_DIR"

      ${concatStringsSep "\n" (mapAttrsToList setupSvc services)}

      echo "Launch!"
      exec ${s6}/bin/s6-svscan "$SVC_DIR"
    '';

  mkSvcScript = op: svc:
    let
      flag = {
        start = "-u -wu";
        stop = "-d -wd";
        restart = "-r -wr";
      }.${op};
    in {
      content = { s6 }: ''
        exec ${s6}/bin/s6-svc ${flag} "$@" ${escapeShellArg cfg.stateDir}/${
          escapeShellArg svc
        }
      '';
    };

  mkRefreshScript = svc: {
    content = ''
      set -eu

      echo "Stopping ${svc}"
      ${config.scripts.materializedGroups.stop.${svc}}

      echo "Cleaning up ${svc}"
      ${config.scripts.materializedGroups.cleanup.${svc}}

      echo "Starting ${svc}"
      ${config.scripts.materializedGroups.start.${svc}}
    '';
  };

  mkServiceDir = let
    builder = { runCommandNoCC }:
      name: config:
      runCommandNoCC "${name}.servicedir" { runSrc = config.run.source; } ''
        mkdir -p $out
        cd $out

        ln -s "$runSrc" run

        ${optionalString config.down ''
          touch down
        ''}
      '';
  in pkgs.callPackage builder { };

  commonHelp =
    "See docs on services directories for more informations: https://skarnet.org/software/s6/servicedir.html";

in {
  options.supervisor = {
    stateDir = mkOption {
      type = types.path;
      default = "${config.meta.stateDir}/supervisor";
      description =
        "Writeable directory used by the supervisor to store the runtime files.";
    };

    serviceDirs = mkOption {
      type = types.attrs;
      default = { };
      internal = true;
      description =
        "key-value pairs for generating service directories, where the name is the name of the directory";
    };

    services = mkOption {
      type = types.attrsOf (types.submodule ({ name, ... }: {
        options = {
          run = mkOption {
            type = lib'.types.script "run-${name}";
            description = "Content of the `run` script. ${commonHelp}";
          };

          down = mkOption {
            type = types.bool;
            default = false;
            description = "Whether to create the `down` file. ${commonHelp}";
          };

          hasCleanupScript = mkOption {
            type = types.bool;
            default = true;
            description =
              "Whether the service has a corresponding script.cleanup.<service>.";
          };
        };

      }));

      description = "The services to be registered in the supervisor.";
      default = { };
    };
  };

  config = {
    supervisor.serviceDirs = mapAttrs mkServiceDir cfg.services;

    scripts.groups = {
      start = genAttrs serviceNames (mkSvcScript "start");
      stop = genAttrs serviceNames (mkSvcScript "stop");
      restart = genAttrs serviceNames (mkSvcScript "restart");
      refresh = genAttrs servicesWithCleanups mkRefreshScript;

      supervisor.cleanup.content = ''
        set -eu

        echo "Removing services directory"
        rm -rf ${escapeShellArg cfg.stateDir}
      '';
    };

    outputs = {
      initScript = pkgs.callPackage mkInitScript {
        inherit (cfg) stateDir;
        services = cfg.serviceDirs;
      };
    };
  };
}
