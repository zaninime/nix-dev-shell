{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.outputs;
  stateDir = config.services.stateDir;

  recommendedLatestVersion = 1;

  checkVersion = let
    extractVersion = pkgs.writeText "extract-version.nix" ''
      { devShell }:
      let
        shellDrv = import devShell;
        version = import "''${shellDrv.repoPath}/version.nix";
      in version.number
    '';

    hook = pkgs.writeText "hook.sh" ''
      _printShellVersion() {
        ${pkgs.nix}/bin/nix-instantiate \
          --eval \
          --argstr devShell "$(pwd)/shell.nix" \
          ${extractVersion}
      }

      checkShellVersion() {
        set -e

        local actual latest
        actual="$(_printShellVersion)"
        latest=${escapeShellArg recommendedLatestVersion}

        if [[ "$actual" -lt "$latest" ]]; then
          echo
          echo "A new version of the dev-shell is available."
          echo
        fi

        set +e
      }
    '';
  in pkgs.makeSetupHook { } hook;

in {
  options.outputs = mkOption {
    type = types.attrs;
    internal = true;
    description =
      "Collection of outputs, to be used by other parts of the system";
  };

  config.outputs = {
    shell = pkgs.mkShellNoCC ({
      buildInputs = (with cfg; [ initScript generatedScripts ])
        ++ (with pkgs; [ cacert ]) ++ [ checkVersion ]
        ++ config.environment.packages;

      repoPath = ../.;

      shellHook = ''
        export PS1='\n\[\033[1;32m\][dev-shell:'${
          pkgs.lib.escapeShellArg config.meta.projectName
        }']\$\[\033[0m\] '

        ${config.environment.shellInit}

        checkShellVersion
      '';
    } // config.environment.variables);
  };
}
