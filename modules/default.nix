{ ... }: {
  imports = [
    ./assertions.nix
    ./environment.nix
    ./lib.nix
    ./meta.nix
    ./nixpkgs.nix
    ./outputs.nix
    ./scripts.nix
    ./supervisor.nix

    ./docs
    ./services
  ];
}
