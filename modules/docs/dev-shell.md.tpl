## OPTIONS
The following options can be used in `shell.nix` file

{{ range . }}
### {{ .name }}

{{ .description }}

*Type*: {{ .type }}  
{{ if index . "default" -}}
*Default*: `{{ .default }}`
{{- end }}

*Source file*:  
{{ index .declarations 0 }}

{{ if index . "example" -}}
*Example*:

```
{{ .example }}
```
{{ end -}}
{{- end }}
