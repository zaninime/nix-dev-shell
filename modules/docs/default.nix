{ pkgs, lib, ... }:

with lib;

let
  overlay = self: super: {
    dev-shell-docs = self.callPackage ./package.nix { };
  };

  banner = ''

    Welcome to the dev-shell!

    Run `help` to bring up the manual or `init` to spin up all enabled services.
  '';

in {
  nixpkgs.overlays = [ overlay ];

  outputs.docs = pkgs.dev-shell-docs;

  environment.packages = with pkgs; [ man less ];
  environment.shellInit = ''
    MANPATH=${lib.escapeShellArg pkgs.dev-shell-docs}/share/man:"$MANPATH"
    export MANPATH

    alias help='man dev-shell.nix'

    echo ${escapeShellArg banner}
  '';
}
