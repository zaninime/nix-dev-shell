{ lib, runCommandNoCC, gomplate, ronn }:

with lib;

let
  evaledModules = let
    # mock values that would otherwise completely block evaluation
    mock = { meta.projectDir = "/<project dir>"; };
  in evalModules { modules = [ ./.. mock ]; };

  documentedOptions = let
    allOptions = optionAttrSetToDocList evaledModules.options;
    validOption = { internal, visible, ... }: !internal && visible;
    printExample = { example ? null, ... }@option:
      if example == null || isString example then
        option
      else
        option // { example = generators.toPretty { } example; };
    fixDeclarations = { declarations ? [ ], ... }@option:
      option // {
        declarations =
          map (removePrefix ((toString ./../..) + "/")) declarations;
      };
  in pipe allOptions [
    (filter validOption)
    (map printExample)
    (map fixDeclarations)
  ];

in runCommandNoCC "dev-shell-docs" {
  nativeBuildInputs = [ gomplate ronn ];

  src = builtins.toJSON documentedOptions;
  passAsFile = [ "src" ];
} ''
  mkdir -p $out/share/man/man5

  # gomplate won't work without an extension
  mv $srcPath{,.json}

  gomplate -c ".=$srcPath.json" -f ${./dev-shell.md.tpl} > dev-shell.nix.5.md

  export HOME="$PWD/home"
  mkdir -p "$HOME"
  ronn --roff dev-shell.nix.5.md

  mv dev-shell.5 $out/share/man/man5/dev-shell.nix.5
''
