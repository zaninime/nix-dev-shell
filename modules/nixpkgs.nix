{ config, lib, ... }:

with lib;

let cfg = config.nixpkgs;

in {
  options.nixpkgs = {
    path = mkOption {
      type = types.path;
      default = (import ../nix/sources.nix).nixpkgs;
      description =
        "Path to the nixpkgs repository, to be used in the modules.";
    };

    overlays = mkOption {
      type = with types; listOf (functionTo (functionTo attrs));
      default = [ ];
      description =
        "Overlays for nixpkgs. Any overlay added here is evaluated before passing `pkgs` to all the modules";
    };
  };

  config = { _module.args.pkgs = import cfg.path { inherit (cfg) overlays; }; };
}
