{ config, lib, ... }:

with lib;

let cfg = config.meta;

in {
  options.meta = {
    projectName = mkOption {
      type = types.str;
      description = "The name (slug) of the project";
    };

    projectDir = mkOption {
      type = types.path;
      description = ''
        The root path of the project. It is recommended to use a string here and not a
        path, ie. "/foo" instead of ./foo. Nix will avoid copying it to the Nix store
        before evaluating it.
      '';
      example = "toString ./.";
    };

    stateDir = mkOption {
      type = types.path;
      default = "${cfg.projectDir}/.tmp";

      description =
        "A writeable directory where all the services can write their data.";
    };
  };

  config = {
    assertions = [{
      assertion =
        builtins.match "([a-z]+-?)([a-z0-9]+-)*[a-z0-9]+" cfg.projectName
        != null && builtins.stringLength cfg.projectName >= 3;
      message = ''
        The meta.projectName = '${cfg.projectName}' option is invalid.

          Allowed format:
          - only lowercase letters and numbers
          - dashes (minuses)
          - can't start or end with a dash
          - can't start with a number
          - minimum length = 3
      '';
    }];
  };
}
