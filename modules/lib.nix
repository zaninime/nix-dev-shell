{ lib, pkgs, ... }:

with lib;

let
  functionType = mkOptionType {
    name = "function";
    check = isFunction;
  };

  scriptModule = ({ name, config, ... }: {
    options = {
      content = mkOption {
        type = with types; nullOr (either str functionType);
        default = null;
        description =
          "Literal content of the script, or function to it (callPackage)";
      };

      source = mkOption {
        type = types.path;
        description = "Path of the source file.";
      };
    };

    config = {
      source = mkIf (config.content != null) (let
        text = if isFunction config.content then
          pkgs.callPackage config.content { }
        else
          config.content;
      in pkgs.writers.writeBash name text);
    };
  });

  customTypes = self: {
    # non-merging function type
    function = functionType;

    script = name:
      types.submodule (args: scriptModule ({ inherit name; } // args));

    script' = types.submodule scriptModule;
  };

  customLib = { types = fix customTypes; };

in { _module.args.lib' = customLib; }
