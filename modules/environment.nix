{ lib, ... }:

with lib;

{
  options.environment = {
    packages = mkOption {
      type = with types; listOf package;
      default = [ ];
      description = "Packages to install in the dev-shell.";
      example = "with pkgs; [ hello ];";
    };

    variables = mkOption {
      type = with types; attrsOf str;
      default = { };
      description = "Environment variables to expose in the dev-shell";
      example = { SVC_PORT = "4000"; };
    };

    shellInit = mkOption {
      type = types.lines;
      default = "";
      description =
        "Lines to append to the `shellHook`, run when loading the shell";
    };
  };
}
