{ lib, config, pkgs, ... }:

with lib;

let
  sources = import ../../nix/sources.nix;
  cfg = config.services.temporal;

  composeFile = let
    dbContainer = "db";
    serverContainer = "server";

    versions = rec {
      postgres = "13";
      server = "1.14.0";
      admin-tools = server;
      web = "1.13.0";
    };

    config = {
      version = "3.5";
      services = {
        ${dbContainer} = {
          environment = {
            POSTGRES_PASSWORD = "temporal";
            POSTGRES_USER = "temporal";
          };
          image = "postgres:${versions.postgres}";
          ports =
            optionals (cfg.dbPort != null) [ "${toString cfg.dbPort}:5432" ];
        };
        ${serverContainer} = {
          depends_on = [ dbContainer ];
          environment = [
            "DB=postgresql"
            "DB_PORT=5432"
            "POSTGRES_USER=temporal"
            "POSTGRES_PWD=temporal"
            "POSTGRES_SEEDS=${dbContainer}"
            "DYNAMIC_CONFIG_FILE_PATH=config/dynamicconfig/development.yaml"
          ];
          image = "temporalio/auto-setup:${versions.server}";
          ports = [ "${toString cfg.serverPort}:7233" ];
          volumes = [ "./dynamicconfig:/etc/temporal/config/dynamicconfig" ];
        };
        admin-tools = {
          depends_on = [ serverContainer ];
          environment = [ "TEMPORAL_CLI_ADDRESS=${serverContainer}:7233" ];
          image = "temporalio/admin-tools:${versions.admin-tools}";
          stdin_open = true;
          tty = true;
        };
        web = {
          depends_on = [ serverContainer ];
          environment = [
            "TEMPORAL_GRPC_ENDPOINT=${serverContainer}:7233"
            "TEMPORAL_PERMIT_WRITE_API=true"
          ];
          image = "temporalio/web:${versions.web}";
          ports = [ "${toString cfg.webPort}:8088" ];
        };
      };
    };
  in pkgs.writeText "docker-compose.yaml" (builtins.toJSON config);

  projectName = "${config.meta.projectName}_temporal";
  composeLocation =
    "${config.supervisor.stateDir}/temporal/data/docker-compose.yaml";

  cleanupScript = { writers, temporal-compose }:
    writers.writeBash "cleanup-temporal" ''
      set -eu

      echo "Deleting containers"
      ${temporal-compose}/bin/temporal-compose down -v --remove-orphans -t 0
    '';

  temporalCompose = { writers, docker-compose }:
    writers.writeBashBin "temporal-compose" ''
      set -eu

      export COMPOSE_PROJECT_NAME=${escapeShellArg projectName}
      export PATH="$PATH:/usr/local/bin"

      exec ${docker-compose}/bin/docker-compose -f ${
        escapeShellArg composeLocation
      } "$@"
    '';

  overlay = self: super: {
    temporal-compose = self.callPackage temporalCompose { };
  };

in {
  options.services.temporal = {
    enable = mkEnableOption "temporal workflow engine";

    serverPort = mkOption {
      type = types.port;
      default = 7233;
      description = "Port of the workflow orchestrator";
    };

    webPort = mkOption {
      type = types.port;
      default = 8088;
      description = "Port of the web interface";
    };

    dbPort = mkOption {
      type = with types; nullOr port;
      default = null;
      description = "Port of postgres";
      example = 5432;
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [ overlay ];

    environment = {
      packages = [ pkgs.temporal-compose ];

      shellInit = ''
        alias tctl='temporal-compose exec admin-tools tctl'
      '';
    };

    supervisor.services.temporal.run.content = { temporal-compose }: ''
      set -eu

      mkdir -p data
      cp -r ${sources.temporalio-compose}/dynamicconfig data/dynamicconfig
      cp ${composeFile} data/docker-compose.yaml
      chmod -R +w data

      exec ${temporal-compose}/bin/temporal-compose up --remove-orphans
    '';

    scripts.groups.cleanup.temporal.content = cleanupScript;
  };
}
