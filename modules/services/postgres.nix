{ lib, config, pkgs, ... }:

with lib;

let cfg = config.services.postgres;

in {
  options.services.postgres = {
    enable = mkEnableOption "postgresql";

    database = mkOption {
      type = types.str;
      description = "The name of the database to create.";
    };

    username = mkOption {
      type = types.str;
      description =
        "The name of the user to create. Local authentication is possible without password (trust).";
    };

    stateDir = mkOption {
      type = types.path;
      description =
        "Writeable directory used by postgres to store the database files.";
      default = "${config.meta.stateDir}/pgdata";
    };

    port = mkOption {
      type = types.port;
      default = 5432;
      description = "Port on which postgres will be listening on.";
    };

    package = mkOption {
      type = types.package;
      default = pkgs.postgresql;
      description =
        "The postgres package to use. It's useful to pin a version which is not the latest available.";
      example = "pkgs.postgres_12";
    };
  };

  config = mkIf cfg.enable {
    supervisor.services.postgres.run.content = ''
      set -eu

      PATH='${makeBinPath [ cfg.package ]}'":$PATH"
      PGDATA=${escapeShellArg cfg.stateDir}

      export PATH PGDATA

      init_files() {
        initdb \
          --username ${escapeShellArg cfg.username}
      }

      start_temp_server() {
        set -- "$@" -c listen_addresses=${"''"} -p 5432

        pg_ctl \
          -D "$PGDATA" \
          -o "$(printf '%q ' "$@")" \
          -w start
      }

      stop_server() {
        pg_ctl \
          -D "$PGDATA" \
          -m fast \
          -w stop
      }

      create_db() {
        echo 'CREATE DATABASE :"db";' | PGHOST= PGHOSTADDR= POSTGRES_DB= psql \
          -v ON_ERROR_STOP=1 \
          --username ${escapeShellArg cfg.username} \
          --no-password \
          --dbname postgres \
          --set db=${escapeShellArg cfg.database}
      }

      if [[ ! -d "$PGDATA" ]]; then
        init_files
        start_temp_server
        create_db
        stop_server
      fi

      trap stop_server SIGTERM
      trap stop_server SIGINT

      postgres --unix_socket_directories= -p ${toString cfg.port} &
      child=$!

      wait "$child"
    '';

    scripts.groups.cleanup.postgres.content = ''
      set -eu

      echo "Removing state directory"
      rm -rf ${escapeShellArg cfg.stateDir}
    '';

    environment = {
      packages = [ cfg.package ];

      shellInit = ''
        alias run-psql='psql -h localhost -p $POSTGRES_PORT -U $POSTGRES_USER -w $POSTGRES_DB'
      '';

      variables = {
        POSTGRES_PORT = toString cfg.port;
        POSTGRES_DB = cfg.database;
        POSTGRES_USER = cfg.username;
      };
    };

    assertions = [
      {
        assertion = cfg.database != "postgres";
        message =
          "Running PostgreSQL with the default database name (postgres) is not supported.";
      }
      {
        assertion = cfg.username != "postgres";
        message =
          "Running PostgreSQL with the default username (postgres) is not supported.";
      }
    ];
  };
}
