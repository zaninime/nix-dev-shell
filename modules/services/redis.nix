{ config, lib, pkgs, ... }:

with lib;

let cfg = config.services.redis;

in {
  options.services.redis = {
    enable = mkEnableOption "redis";

    port = mkOption {
      type = types.port;
      default = 6379;
      description = "The port on which redis will be listening on.";
    };

    stateDir = mkOption {
      type = types.path;
      description = "Writeable directory used by redis to store the snapshots.";
      default = "${config.meta.stateDir}/redis";
    };

    package = mkOption {
      type = types.package;
      description = "The redis package to use.";
      default = pkgs.redis;
    };
  };

  config = mkIf cfg.enable {
    supervisor.services.redis.run.content = ''
      set -eu

      state_dir=${escapeShellArg cfg.stateDir}

      mkdir -p "$state_dir"

      exec ${cfg.package}/bin/redis-server \
        --port ${toString cfg.port} \
        --dir "$state_dir"
    '';

    scripts.groups.cleanup.redis.content = ''
      set -eu

      echo "Removing state directory"
      rm -rf ${escapeShellArg cfg.stateDir}
    '';

    environment = {
      packages = [ cfg.package ];

      shellInit = ''
        alias run-redis-cli='redis-cli -p $REDIS_PORT'
      '';

      variables.REDIS_PORT = toString cfg.port;
    };
  };
}
