{ config, options, lib, pkgs, ... }:

with lib;

let cfg = config.services.localstack;

in {
  options.services.localstack = {
    enable = mkEnableOption "localstack";

    image = mkOption {
      type = types.str;
      default = "localstack/localstack:latest";
      description =
        "Docker image to use. It is recommended to use a specific tag or sha256 instead of the default `latest`.";
    };

    edgePort = mkOption {
      type = types.port;
      default = 4566;
      description =
        "Which port should be exposed to localhost for the edge router (common ingress for all services)";
    };

    configuration = mkOption {
      type = with types; attrsOf str;
      default = { };
      description =
        "Set of environment variables to pass to LocalStack. See: https://github.com/localstack/localstack#configurations";
      example = { SERVICES = "s3"; };
    };
  };

  config = mkIf cfg.enable {
    supervisor.services.localstack = {
      run.content = { stdenv, docker }:
        let
          envFlags = pipe cfg.configuration [
            (mapAttrsToList
              (n: v: "-e ${escapeShellArg n}=${escapeShellArg v}"))
            (concatStringsSep " ")
          ];

          dockerBin = if stdenv.isDarwin then
            "/usr/local/bin/docker"
          else
            "${docker}/bin/docker";
        in ''
          set -eu

          _term() {
            echo "Killing container $container_id"
            ${dockerBin} kill "$container_id"
          }

          trap _term SIGTERM
          trap _term SIGINT

          container_id="$(${dockerBin} run -d --rm -p ${
            toString cfg.edgePort
          }:4566 ${envFlags} ${cfg.image})"

          echo "Started localstack container $container_id"

          echo "Attaching logs"
          ${dockerBin} logs -f "$container_id" &
          logs_pid=$!

          wait "$logs_pid"
        '';

      hasCleanupScript = false;
    };

    warnings = mkIf (cfg.image == options.services.localstack.image.default) [''

      You will be running localstack with the default image specification (latest).
      This is not reproducible across machines. It is recommended to switch to a tag
      or a sha256 instead. Use:

        services.localstack.image = "...";

      in your config to do so.
    ''];
  };
}
