{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.kafka;

  propertiesFile = let
    content = {
      "advertised.listeners" = "PLAINTEXT://localhost:${toString cfg.nodePort}";
      "controller.listener.names" = "CONTROLLER";
      "controller.quorum.voters" = "1@localhost:${toString cfg.controllerPort}";
      "inter.broker.listener.name" = "PLAINTEXT";
      "listener.security.protocol.map" =
        "CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT,SASL_SSL:SASL_SSL";
      listeners = "PLAINTEXT://:${toString cfg.nodePort},CONTROLLER://:${
          toString cfg.controllerPort
        }";
      "log.dirs" = cfg.stateDir;
      "log.retention.check.interval.ms" = "300000";
      "log.retention.hours" = "168";
      "log.segment.bytes" = "1073741824";
      "node.id" = "1";
      "num.io.threads" = "8";
      "num.network.threads" = "3";
      "num.partitions" = "1";
      "num.recovery.threads.per.data.dir" = "1";
      "offsets.topic.replication.factor" = "1";
      "process.roles" = "broker,controller";
      "socket.receive.buffer.bytes" = "102400";
      "socket.request.max.bytes" = "104857600";
      "socket.send.buffer.bytes" = "102400";
      "transaction.state.log.min.isr" = "1";
      "transaction.state.log.replication.factor" = "1";
    };
  in pkgs.writeText "server.properties" (lib.generators.toKeyValue { } content);

in {
  options.services.kafka = {
    enable = mkEnableOption "kafka";

    controllerPort = mkOption {
      type = types.port;
      default = 9093;
      description =
        "The port on which the kafka controller will be listening on.";
    };

    nodePort = mkOption {
      type = types.port;
      default = 9092;
      description = "The port on which the kafka node will be listening on.";
    };

    stateDir = mkOption {
      type = types.path;
      description = "Writeable directory used by kafka to store the logs";
      default = "${config.meta.stateDir}/kafka";
    };

    package = mkOption {
      type = types.package;
      description = "The kafka package to use.";
      default = pkgs.apacheKafka_2_8;
    };
  };

  config = mkIf cfg.enable {
    supervisor.services.kafka.run.content = ''
      set -eu

      export PATH=${makeBinPath [ cfg.package ]}":$PATH"

      state_dir=${escapeShellArg cfg.stateDir}
      cluster_format_status_file="$state_dir/ready"

      mkdir -p "$state_dir"

      if [[ ! -f "$cluster_format_status_file" ]]; then
        echo "Generating new cluster ID"
        cluster_id="$(kafka-storage.sh random-uuid)"

        echo "Formatting logs directory"
        kafka-storage.sh format -c ${propertiesFile} -t "$cluster_id"
        touch "$cluster_format_status_file"
      fi

      echo "Starting kafka server"
      exec kafka-server-start.sh ${propertiesFile}
    '';

    scripts.groups.cleanup.kafka.content = ''
      set -eu

      echo "Removing state directory"
      rm -rf ${escapeShellArg cfg.stateDir}
    '';

    environment = { packages = [ cfg.package ]; };
  };
}
