{ lib, lib', config, pkgs, ... }:

with lib;

let
  cfg = config.scripts;

  configuredScripts = pipe cfg.groups [
    (mapAttrsRecursiveCond (a: !(a ? source)) (p: v: rec {
      type = "link";
      scriptGroup = elemAt p 0;
      scriptName = elemAt p 1;
      path =
        v.source.overrideAttrs (_: { name = "${scriptGroup}-${scriptName}"; });
    }))
    (collect (x: (x.type or null) == "link"))
  ];

  mapToFarm = map ({ scriptGroup, scriptName, path, ... }: {
    name = "bin/${scriptGroup}-${scriptName}";
    inherit path;
  });

  materializedGroups = pipe configuredScripts [
    (map ({ scriptGroup, scriptName, path, ... }: {
      ${scriptGroup}.${scriptName} = path;
    }))
    (foldr recursiveUpdate { })
  ];

  groupedScriptNames = mapAttrs (const attrNames) cfg.groups;

  allScripts = pipe groupedScriptNames [
    (filterAttrs (_: v: v != [ ]))
    (mapAttrsToList (n: v: {
      scriptGroup = n;
      scriptName = "all";
      path = pkgs.writers.writeBash "${n}-all" ''
        set -eu

        ${concatMapStringsSep "\n" (s: ''
          echo Executing ${n} ${s}
          ${materializedGroups.${n}.${s}}
          echo
        '') v}
      '';
    }))
  ];

in {
  options.scripts = {
    groups = mkOption {
      type = with types; attrsOf (attrsOf lib'.types.script');
      default = { };
      description = ''
        Scripts to be added to the dev-shell, in the format <group>.<name> = <path>.

        The module will create a symlink bin/<group>-<name> to <path> for each declared
        script.

        Furthermore a bin/<group>-all script is created and calls all the scripts in
        the group.
      '';
    };

    materializedGroups = mkOption {
      type = with types; attrsOf (attrsOf path);
      internal = true;
    };
  };

  config = mkIf true {
    scripts.materializedGroups = materializedGroups;
    outputs.generatedScripts =
      pkgs.linkFarm "scripts" (mapToFarm (configuredScripts ++ allScripts));
  };
}
