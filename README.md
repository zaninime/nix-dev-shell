# Nix powered dev environment

This repo will help you set-up a nix-shell for your project (ie. a microservice).

This project is based on [nix](https://nixos.org/manual/nix/stable/#chap-introduction), a purely functional package manager. The easiest way to install it on MacOS is:

```shell
curl -L https://nixos.org/nix/install | sh -s -- --darwin-use-unencrypted-nix-store-volume
```

Please be aware that this will not work on old Macbook models (eg: Mid 2015), have a look at [Nix guide](https://nixos.org/manual/nix/stable/#sect-macos-installation) for more options.

## How to integrate

### One-liner
The easiest and recommended way to get a nix-shell is to run the installer in the root directory of your project:

```shell
sh <(curl https://gitlab.personio-internal.de/personio-public/nix-dev-shell/-/raw/master/installer/stage1.sh)
```

It will generate two files:
- `shell.nix`: this is the brain of your nix-shell and where you declare all the needed services
- `dev-shell`: a handy wrapper around `nix-shell --pure`

### Manual way

Please create a file named `shell.nix` in your project's root directory and add the following content:
```nix
let
  devShell = builtins.fetchTarball "https://gitlab.com/zaninime/nix-dev-shell/-/archive/master/nix-dev-shell-master.tar.gz";

in (import ("${devShell}/mk-shell.nix") ({ pkgs, ... }: {
  meta = {
    projectName = "my-shiny-microservice";
    projectDir = toString ./.;
  };
}))
```

This is the minimal configuration you can have and by default it will have no service available.

## First steps

Once you have your `shell.nix` file, run `nix-shell --pure`. After some installation, you should see the following welcome message:

> Welcome to the dev-shell! Please run `help` to bring up the manual or `init` to spin up all configured services

As it suggests, please run `help` and have a look at all possible, up-to-date configurations available.

## Why should I use it?

Nix doesn't use any kind of virtualization, but guarantees perfect reproducibility and is much more lightweight than docker.

Each service will be specific to your project, mirroring the same setup we have in our cluster, however all software will be shared.
Thanks to `nix`, using different versions of the same software will never be an issue.

Because all services run on your local machine, you will be able to access them using normal tools like DBeaver. Inside the nix shell we will provide utilities such as `redis-cli` and `psql` (but if and only if we enabled those services).

## Examples

It is pretty common for our services to use postgres:
```nix
let
  devShell = builtins.fetchTarball "https://gitlab.com/zaninime/nix-dev-shell/-/archive/master/nix-dev-shell-master.tar.gz";

in (import ("${devShell}/mk-shell.nix") ({ pkgs, ... }: {
  services.postgres = {
    enable = true;
    database = "my-shiny-db";
    username = "my-shiny-admin";
    port = 5432;
    package = pkgs.postgresql_12;
  };
  
  meta = {
    projectName = "my-shiny-microservice";
    projectDir = toString ./.;
  };
}))
```

Once you run `nix-shell --pure`, it will download postgres 12 if it's not already available. Then you can run `init` to start all configured services.

Postgres 12 will run on our local machine and will be accessible on `localhost` at port `5432`. All data will be stored by default in `<project-root>/.tmp/pgdata`.

You can `CTRL+C` the `init` command to bring down all services; once done `CTRL+C` again to fully exit the process. Alternatively you can run `stop-all` to bring down all services.

### Extra tools
Because we enabled postgres, we have some extra tools available inside the nix shell:

- `refresh-postgres`: it will stop the service, delete all data and spin it up again
- `start-postgres`: start the service
- `stop-postgres`: stop the service
- `restart-postgres`: stop & start service
- `cleanup-postgres`: delete all data related to this service
- `run-psql`: an alias that will run `psql` with the right credentials

It is worth noticing that `refresh-all`, `start-all`, `stop-all`, `restart-all` and `cleanup-all` are always available and will apply to all enabled services.

Please be aware that you can run `./dev-shell` multiple times. We recommend to keep `init` in one shell and then open another one if you need to run other commands.
