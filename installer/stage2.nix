let
  sources = import ../nix/sources.nix;
  pkgs = import sources.nixpkgs { };

  inherit (pkgs) writers;

in writers.writeBash "installer-stage2" ''
  set -eu

  echo "Welcome to the dev-shell installer!"
  echo

  echo "This command will install the dev-shell in the current directory:"
  pwd
  echo

  read -p "Would you like to proceed [yn]? " -n 1 -r
  echo

  if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    echo "Aborting."
    exit 1
  fi

  echo
  echo "Installing files..."

  if [[ -f shell.nix ]]; then
    echo "- (skipped) shell.nix: file present"
  else
    cp ${./files/shell.nix} shell.nix
    echo "- shell.nix"
  fi

  echo "- dev-shell"
  cp ${./files/dev-shell} dev-shell

  echo
  echo -n "Fixing permissions..."
  chmod +w dev-shell shell.nix
  echo " OK"

  echo
  echo "Setup complete. Have fun!"
''
