#!/usr/bin/env bash

set -eu

echo "Checking if nix is installed..."

if ! command -v nix &> /dev/null; then
  echo
  echo "Nix is required to run the install. Please install nix first."
  exit 1
fi

echo -n " OK"
echo

NIX_EXPR=$(cat <<'EOF'
let
  repo = builtins.fetchTarball {
    url =
      "https://gitlab.com/zaninime/nix-dev-shell/-/archive/master/nix-dev-shell-master.tar.gz";
  };
in import "${repo}/installer/stage2.nix"
EOF
)

echo "Preparing the second stage..."

stage2="$(nix-build --no-out-link -E "$NIX_EXPR" --show-trace)"

echo
echo "Launching it..."
echo
echo "----------------------------------------"
echo

exec "$stage2"
