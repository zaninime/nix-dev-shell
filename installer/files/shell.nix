let
  devShell = builtins.fetchTarball {
    url =
      "https://gitlab.com/zaninime/nix-dev-shell/-/archive/v1/nix-dev-shell-v1.tar.gz";
  };

in (import ("${devShell}/mk-shell.nix") ({ pkgs, ... }: {
  # services.postgres = {
  #   enable = true;

  #   username = "database-name";
  #   database = "database-name";

  #   # check docs for the default version
  #   package = pkgs.postgresql_12;

  #   # optional: customize the port
  #   port = 5433;
  # };

  # services.redis = {
  #   enable = true;

  #   # optional: customize the port
  #   port = 6000;
  # };

  # services.localstack = {
  #   enable = true;

  #   # customize the edge router port
  #   edgePort = 4566;

  #   # optional: customize the services and any other config accepted by localstack
  #   configuration.SERVICES = "s3,sqs";
  # };

  meta = {
    # mandatory: the name of your project, in a slugified form.
    projectName = "NO_NAME";

    # change this only if you know what you're doing
    projectDir = toString ./.;
  };
}))
